const rollup = require("rollup");
const babel = require("rollup-plugin-babel");

rollup
  .rollup({
    input: "src/index.js",
    plugins: [
      babel({
        exclude: "node_modules/**"
      }),
    ]
  })
  .then(bundle =>
    bundle.write({
      file: "build/index.js",
      format: "cjs"
    })
  );
