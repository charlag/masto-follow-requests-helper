//@flow
import R from "ramda";
import Mastodon from "mastodon-api";
import { readJSON } from "./utils.js";

type ClientConfig = {
  access_token: string,
  url: string
};

export type Emoji = {
  shortcode: string,
  static_url: string,
  url: string,
  visible_in_picked: boolean
};

export type Account = {
  id: string,
  username: string,
  acct: string,
  display_name: string,
  locked: boolean,
  created_at: Date,
  followers_count: number,
  following_count: number,
  statuses_count: number,
  note: string,
  url: string,
  avatar: string,
  avatar_static: string,
  header: string,
  emojis: Array<Emoji>,
  moved: ?boolean,
  fields: Array<{ name: string, value: string }>,
  bot: ?boolean
};

const getConfig = async (): Promise<ClientConfig> => {
  const config = await readJSON("config.json");

  if (
    config == null ||
    typeof config.url != "string" ||
    typeof config.access_token != "string" ||
    config.access_token.trim() == ""
  ) {
    throw new Error("Invalid config values");
  } else {
    return config;
  }
};

export const getMastodonClient = async (): Promise<Mastodon> => {
  const config = await getConfig();
  return new Mastodon({
    access_token: config.access_token,
    api_url: config.url + "/api/v1/"
  });
};

const processResponse = (response: IncomingMessage): any => {
  if (response.data == null) {
    throw new Error("Empty response");
  } else if (response.data.error != null) {
    throw new Error(response.data.error);
  }
  return response.data;
};

export const getFollowRequests = async (client: Mastodon) => {
  const response = await client.get("follow_requests", { limit: 79 });
  return processResponse(response);
};

export const getRelationshipts = async (
  client: Mastodon,
  idOrIds: string | Array<string>
) => {
  return processResponse(
    await client.get("accounts/relationships", {
      id: idOrIds
    })
  );
};

export const approveFollowRequest = (
  client: Mastodon,
  request: Account
): Promise<*> => client.post(`follow_requests/${request.id}/authorize`);
