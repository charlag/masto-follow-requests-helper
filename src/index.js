//@flow
import * as R from "ramda";
import readline from "readline";
import { exec } from "child_process";

import {
  getMastodonClient,
  getFollowRequests,
  approveFollowRequest,
  getRelationshipts
} from "./client.js";

const rd = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const question = query =>
  new Promise(resolve => {
    rd.question(query, resolve);
  });

const findFollowing = (requests, relationships) =>
  R.pipe(
    R.zip,
    R.filter(([_, ship]) => ship.following),
    R.map(R.head)
  )(requests, relationships);

const approve = (client, request) =>
  approveFollowRequest(client, request)
    .then(() => console.log(`✅ Accepted ${request.url}\n`))
    .catch(e => console.error(`❗️ Failed to accept ${request.url}`));

const askToApprove = (client, request) =>
  question(`Should approve ${request.url} y/N/o(pen)? `).then(response => {
    if (response == null) return;
    const tidyResponse = response.trim().toLowerCase();
    if (tidyResponse === "y") {
      return approve(client, request);
    } else if (tidyResponse == "o") {
      return exec(`xdg-open ${request.url}`), askToApprove(client, request);
    }
  });

const start = async () => {
  const mastodonClient: Mastodon = await (getMastodonClient().catch(e => {
    console.error("Error while reading config: " + e);
    process.exit(1);
    return (null: any)
  }));

  const requests = await getFollowRequests(mastodonClient);
  console.log(`${requests.length} requests`);
  const relationships = await getRelationshipts(
    mastodonClient,
    requests.map(r => r.id)
  );

  const toFollow = findFollowing(requests, relationships);

  for (let toFollowRequest of toFollow) {
    await askToApprove(mastodonClient, toFollowRequest);
  }
  console.log("Done");
  process.exit(0);
};

start();
