//@flow
import R from "ramda";
import { promisify } from "util";
import fs from "fs";

export const readP: string => Promise<Buffer> = promisify(fs.readFile);

export const readString = async (filename: string): Promise<string> =>
  (await readP(filename)).toString();

export const readJSON = (filename: string): Promise<any> =>
  readString(filename).then(JSON.parse);
