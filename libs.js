import type { IncomingMessage } from "http";

declare type MastodonConfig = {
  access_token: string
};

type RequestParams = { [string]: mixed };

declare class Mastodon {
  constructor(MastodonConfig): Mastodon;

  get(path: string, params?: { [string]: mixed }): Promise<IncomingMessage>;

  post(path: string, params?: RequestParams): Promise<IncomingMessage>;
}
