const { getMastodonClient } = require("./client.js");
const { readString } = require("./utils.js");

const FILENAME = "log_18-09-2018-fixed";

const readUsernames = () => readString(FILENAME);

Promise.all([readUsernames(), getMastodonClient()]).then(
  async ([loggedUsernamesString, client]) => {
    const usernames = new Set(loggedUsernamesString.split("\n"));
    const requests = (await client.get("follow_requests", {
      limit: 79
    })).data.map(r => r.acct);
    const wereNotified = requests.filter(r => usernames.has(r));
    console.log("Were notified: \n" + wereNotified.join("\n"));
  }
);
